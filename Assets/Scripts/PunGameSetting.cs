﻿using UnityEngine;

public class PunGameSetting
{
    public const string PLAYER_COLOR = "PlayerColor";
    public const string PLAYER_1_SCOREKEY = "Player 1 Score";
    public const string PLAYER_2_SCOREKEY = "Player 2 Score";
    public const string PLAYER_3_SCOREKEY = "Player 3 Score";
    public const string PLAYER_4_SCOREKEY = "Player 4 Score";
    public const string PLAYER_WEAPON = "PlayerWeapon";
    
}
