using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Photon.Pun;
using Photon.Pun.Demo.Procedural;
using Photon.Realtime;

public class CharacterController : MonoBehaviourPunCallbacks, IPunObservable
{
    
    [SerializeField] private float speed = 5f;
    [SerializeField] private float jumpForce = 2;
    float inputX = 0;
    [SerializeField] private Rigidbody2D rb = null;
    
    [Tooltip("Is Knockingback")]
    public bool unbalance = false;
    public bool isBlock = false;
    
    [Tooltip("Block WaitTime")]
    [SerializeField] private float waitTime = 0;
    public float blockInterval = 1.0f;
    private float _blockTime = 1f;
    public GameObject blockSprite;

    
    
    
    [SerializeField] private WeaponStat weaponStat;
    [SerializeField] private BulletStat bulletStat;
    
    private Vector3 _movement;
    
    #region MonoBehaviour

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (!photonView.IsMine)
        {
            OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
        }
    }

    private void Update()
    {
        if (photonView.IsMine)
        {
            if (this.rb.velocity.x > speed ||
                this.rb.velocity.x < -speed)
            {
                unbalance = true;
            }
            else
            {
                unbalance = false;
            }


            /*animator.SetBool (WALK_PROPERTY,
              Math.Abs (_movement.sqrMagnitude) > Mathf.Epsilon);*/
            ProcessInput();
            TimeCount();
        }
    }

    private void ProcessInput()
    {
        //Get Input
        
        //float inputY = 0;

        inputX = Input.GetAxisRaw("Horizontal");
        //inputY = Input.GetAxisRaw("Vertical");
        // Normalize
        _movement = new Vector3(inputX, 0, 0).normalized;

        //Block
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Block();
        }

        

        if (Input.GetButtonDown("Jump") && Mathf.Abs(rb.velocity.y) < 0.001f)
        {
            rb.AddForce(new Vector2(0,jumpForce), ForceMode2D.Impulse);
        }
    }

    

    private void FixedUpdate()
    {
        if (!unbalance)
            Move();
    }

    //Get Hit!
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!photonView.IsMine)
            return;
        
        if(isBlock == false)
        {
            if (other.gameObject.CompareTag("Weapon"))
            {
                if (other.transform.root != transform.root)
                {
                    weaponStat = other.GetComponent<WeaponStat>();
                    unbalance = true;
                    rb.AddForce(other.transform.up * weaponStat.attackForce, ForceMode2D.Impulse);
                    Debug.Log("Hit! Force: " + weaponStat.attackForce);
                    Debug.Log("Is Block = " + isBlock);
                }
            }

            if (other.gameObject.CompareTag("Bullet"))
            {
                if (other.transform.root != transform.root)
                {
                    bulletStat = other.GetComponent<BulletStat>();
                    unbalance = true;
                    rb.AddForce(other.transform.up * bulletStat.attackForce, ForceMode2D.Impulse);
                    //Debug.Log("Hit! Force: " + weaponStat.attackForce);
                    Debug.Log("Is Block = " + isBlock);
                }
            }
        }
    }

    private void TimeCount() //Main Timer
    {
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
        else
        {
            waitTime = 0;
        }
        
        if (isBlock)
        {
            _blockTime -= Time.deltaTime;
        }

        if (_blockTime <= 0)
        {
            Debug.Log("unblock");
            isBlock = false;
            photonView.RPC("HideBlockSprite", RpcTarget.All);
            //ShowBlockSprite();
            _blockTime = 0.8f;
        }
    }

    //Block
    private void Block()
    {
        if (waitTime <= 0 && !isBlock)
        {
            Debug.Log("block");
            isBlock = true;
            photonView.RPC("ShowBlockSprite", RpcTarget.All);
            //ShowBlockSprite();
            waitTime = blockInterval;
        }
    }
    [PunRPC]
    public void ShowBlockSprite()
    {
        blockSprite.SetActive(true);
    }

    [PunRPC]
    public void HideBlockSprite()
    {
        blockSprite.SetActive(false);
    }
    private void Move()
    {
        rb.velocity = new Vector3(_movement.x * speed, rb.velocity.y);
    }

    #endregion

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(isBlock);
            //stream.SendNext(blockSprite.active);
        }
        else
        {
            isBlock = (bool) stream.ReceiveNext();
            //blockSprite.active = (bool) stream.ReceiveNext();
        }
    }
}